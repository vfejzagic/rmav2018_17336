package ba.unsa.etf.rma.vedad.rma17_17336;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by vedo on 3/22/18.
 */

public class KonekcijaBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        if(netInfo == null){
            Toast.makeText(context, "Nema interneta", Toast.LENGTH_SHORT).show();
        } else {
            NetworkInfo.State state = netInfo.getState();
            if(state == state.CONNECTED){
                Toast.makeText(context, "Uredjaj je konektovan", Toast.LENGTH_SHORT).show();
            }
        }


    }
}
