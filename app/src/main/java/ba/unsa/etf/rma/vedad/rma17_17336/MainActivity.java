package ba.unsa.etf.rma.vedad.rma17_17336;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import static ba.unsa.etf.rma.vedad.rma17_17336.MuzicarDBOpenHelper.MUZICAR_ID;
import static ba.unsa.etf.rma.vedad.rma17_17336.MuzicarDBOpenHelper.MUZICAR_IME;
import static ba.unsa.etf.rma.vedad.rma17_17336.MuzicarDBOpenHelper.MUZICAR_ZANR;

public class MainActivity extends AppCompatActivity implements FragmentLista.OnItemClick{

    Boolean siril = false;
    final ArrayList<Muzicar> muzicari = new ArrayList<Muzicar>();
    Boolean switchClicked = false;
    Muzicar mKliknut;
    MuzicarDBOpenHelper muzicarDBOpenHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



/*
        Button btn = (Button) findViewById(R.id.button);
        EditText editText = (EditText) findViewById(R.id.editText);
        ListView listView = (ListView) findViewById(R.id.listView);

        final ArrayList<Muzicar> muzicari = new ArrayList<Muzicar>();

        Zanr rock = new Zanr("Rock", BitmapFactory.decodeResource(getResources(),R.drawable.rock));
        Zanr blues = new Zanr("Blues", BitmapFactory.decodeResource(getResources(), R.drawable.blues));

        muzicari.add(new Muzicar("Elvis", "Presley", rock, "Elvis Presley je rock muzicar", "https://www.elvis.com",
                new ArrayList<String>() {{
                    add("Jailhouse Rock");
                    add("It's now or never");
                    add("Hound Dog");
                    add("Don't");
                    add("An American Trilogy");
        }}));
        muzicari.add(new Muzicar("Bob", "Dylan", blues, "Bob Dylan je blues muzicar", "https://www.bobdylan.com/",
                new ArrayList<String>(){{
                    add("Like a Rolling Stone");
                    add("Forever Young");
                    add("Hurricane");
                    add("Idiot Wind");
                    add("Masters of War");
                }}));

        MuzicarAdapter adapterMuzicar = new MuzicarAdapter(this, R.layout.element_liste, R.id.Itemname, R.id.icon, muzicari);
        listView.setAdapter(adapterMuzicar);
        adapterMuzicar.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, DetaljiMuzicar.class);
                intent.putExtra("ime", muzicari.get(i).getIme());
                intent.putExtra("prezime", muzicari.get(i).getPrezime());
                intent.putExtra("bio", muzicari.get(i).getBiografija());
                intent.putExtra("website", muzicari.get(i).getWebsite());
                intent.putStringArrayListExtra("pjesme", muzicari.get(i).getPjesme());
                MainActivity.this.startActivity(intent);
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "TEST ACTION SEND");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        editText.setText(getIntent().getStringExtra(Intent.EXTRA_TEXT));
*/


        Zanr rock = new Zanr("Rock", BitmapFactory.decodeResource(getResources(),R.drawable.rock));
        Zanr blues = new Zanr("Blues", BitmapFactory.decodeResource(getResources(), R.drawable.blues));

        muzicari.add(new Muzicar("Elvis", "Presley", rock, "Elvis Presley je rock muzicar", "https://www.elvis.com",
                new ArrayList<String>() {{
                    add("Jailhouse Rock");
                    add("It's now or never");
                    add("Hound Dog");
                    add("Don't");
                    add("An American Trilogy");
                }}));
        muzicari.add(new Muzicar("Bob", "Dylan", blues, "Bob Dylan je blues muzicar", "https://www.bobdylan.com/",
                new ArrayList<String>(){{
                    add("Like a Rolling Stone");
                    add("Forever Young");
                    add("Hurricane");
                    add("Idiot Wind");
                    add("Masters of War");
                }}));

        FragmentManager fm = getFragmentManager();
        FrameLayout ldetalji = (FrameLayout)findViewById(R.id.mjestoF2);

        if(ldetalji!=null){
            siril = true;
            FragmentDetalji fd;
            fd = (FragmentDetalji) fm.findFragmentById(R.id.mjestoF2);
            if(fd == null){
                fd = new FragmentDetalji();
                fm.beginTransaction().replace(R.id.mjestoF2, fd).commit();


            }
        }

        FrameLayout ltreci = (FrameLayout) findViewById(R.id.mjestoF3);
        if(ltreci != null){
            FragmentPjesme fp = (FragmentPjesme) fm.findFragmentById(R.id.mjestoF3);
            if(fp == null){
                fp = new FragmentPjesme();
                fm.beginTransaction().replace(R.id.mjestoF3, fp).commit();
            }
        }

        FragmentLista f1 = (FragmentLista) fm.findFragmentByTag("Lista");
        if(f1 == null){
            f1 = new FragmentLista();
            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("Alista", muzicari);
            f1.setArguments(argumenti);
            fm.beginTransaction().replace(R.id.mjestoF1, f1, "Lista").commit();
        }
        else{
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }


    }

    @Override
    public void onItemClicked(int pos){
        Bundle arguments = new Bundle();
        mKliknut = muzicari.get(pos);
        ArrayList<Muzicar> slicni = new ArrayList<Muzicar>();
        if(mKliknut != null){
            for(int i = 0; i < muzicari.size(); i++)
                if(muzicari.get(i).getZanr() == mKliknut.getZanr())
                    slicni.add(muzicari.get(i));
        }
        arguments.putParcelable("muzicar", mKliknut);
        FragmentDetalji fd = new FragmentDetalji();
        FragmentPjesme fp = new FragmentPjesme();
        FragmentSlicniMuzicari fsm = new FragmentSlicniMuzicari();
        fd.setArguments(arguments);
        fp.setArguments(arguments);
        Bundle argMuzicari = new Bundle();
        argMuzicari.putParcelableArrayList("sviMuzicari", slicni);
        fsm.setArguments(argMuzicari);
        if(siril){
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, fd).commit();
            getFragmentManager().beginTransaction().replace(R.id.mjestoF3, fp).commit();

            if(switchClicked) getFragmentManager().beginTransaction().replace(R.id.mjestoF3, fsm).commit();
            else getFragmentManager().beginTransaction().replace(R.id.mjestoF3, fp).commit();

        }
        else{

            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            transaction.replace(R.id.mjestoF1, fd).addToBackStack(null);
            if(switchClicked) transaction.add(R.id.mjestoF18, fsm);
            else  transaction.add(R.id.mjestoF18, fp);
            transaction.commit();
        }
    }

    @Override
    public void onSwitchClicked(Boolean clicked){
        switchClicked = clicked;
        Bundle arguments = new Bundle();
        ArrayList<Muzicar> slicni = new ArrayList<Muzicar>();
        if(mKliknut != null){
            for(int i = 0; i < muzicari.size(); i++)
                if(muzicari.get(i).getZanr() == mKliknut.getZanr())
                    slicni.add(muzicari.get(i));
        }
        arguments.putParcelable("muzicar", mKliknut);
        FragmentPjesme fp = new FragmentPjesme();
        fp.setArguments(arguments);

        Bundle argMuzicari = new Bundle();
        argMuzicari.putParcelableArrayList("sviMuzicari", slicni);
        FragmentSlicniMuzicari fsm = new FragmentSlicniMuzicari();
        fsm.setArguments(argMuzicari);

        if(siril && mKliknut != null){

            if(clicked) getFragmentManager().beginTransaction().replace(R.id.mjestoF3, fsm).commit();
            else getFragmentManager().beginTransaction().replace(R.id.mjestoF3, fp).commit();
        }
    }


}
