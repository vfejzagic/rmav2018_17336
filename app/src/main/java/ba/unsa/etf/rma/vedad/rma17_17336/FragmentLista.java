package ba.unsa.etf.rma.vedad.rma17_17336;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static ba.unsa.etf.rma.vedad.rma17_17336.MuzicarDBOpenHelper.DATABASE_NAME;
import static ba.unsa.etf.rma.vedad.rma17_17336.MuzicarDBOpenHelper.DATABASE_VERSION;
import static ba.unsa.etf.rma.vedad.rma17_17336.MuzicarDBOpenHelper.MUZICAR_ID;
import static ba.unsa.etf.rma.vedad.rma17_17336.MuzicarDBOpenHelper.MUZICAR_IME;
import static ba.unsa.etf.rma.vedad.rma17_17336.MuzicarDBOpenHelper.MUZICAR_ZANR;

public class FragmentLista extends Fragment implements SearchArtist.OnMuzicarSearchDone{

    private ArrayList<Muzicar> muzicari;
    private OnItemClick oic;
    private MuzicarAdapter adapter;
    private boolean clicked = false;
    private Muzicar muzicar;
    private MuzicarDBOpenHelper muzicarDBOpenHelper;

    public FragmentLista(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.lista_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        if(getArguments().containsKey("Alista")){
            muzicari = getArguments().getParcelableArrayList("Alista");
            ListView lv = (ListView)getView().findViewById(R.id.listView);
            adapter = new MuzicarAdapter(getActivity(), R.layout.element_liste, muzicari);
            Button btn = (Button) getView().findViewById(R.id.button);
            final EditText srch = (EditText) getView().findViewById(R.id.editText);


            muzicarDBOpenHelper = new MuzicarDBOpenHelper(getActivity().getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
            ContentValues novi = new ContentValues();
            novi.put(MUZICAR_IME, "TEST");
            novi.put(MUZICAR_ZANR, "pop");
            SQLiteDatabase db = muzicarDBOpenHelper.getWritableDatabase();
            db.insert(MuzicarDBOpenHelper.DATABASE_TABLE, null, novi);

            String[] koloneRezultat = new String[]{MUZICAR_ID, MUZICAR_IME, MUZICAR_ZANR};
            String name = "'TEST'";
            String where = MUZICAR_IME + "=" + name ;

            String whereArgs[] = null;
            String groupBy = null;
            String having = null;
            String order = null;

            Cursor cursor = db.query(MuzicarDBOpenHelper.DATABASE_TABLE, koloneRezultat, where,
                    whereArgs, groupBy, having, order);

            MuzicarCursorAdapter muzicarCursorAdapter = new MuzicarCursorAdapter(getActivity(),
                    R.layout.element_liste, cursor, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

            int INDEX_KOLONE_IME = cursor.getColumnIndexOrThrow(MUZICAR_IME);

            while(cursor.moveToNext())
                Log.d("Ime: ", cursor.getString(INDEX_KOLONE_IME));

            cursor.close();

            lv.setAdapter(muzicarCursorAdapter);
            //lv.setAdapter(adapter);

            final Button switchbtn = (Button) getView().findViewById(R.id.buttonSwitch);
            switchbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!clicked) switchbtn.setText(R.string.switch_artists);
                    else switchbtn.setText(R.string.switch_songs);

                    clicked = !clicked;
                    oic.onSwitchClicked(clicked);
                }
            });

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SearchArtist((SearchArtist.OnMuzicarSearchDone)FragmentLista.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, srch.getText().toString());
                }
            });

            try{
                oic = (OnItemClick)getActivity();
            }
            catch(ClassCastException e){
                throw new ClassCastException(getActivity().toString()+"Treba implementirati OnItemClick");
            }

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    oic.onItemClicked(i);
                }
            });


        }


    }

    public interface OnItemClick{
        public void onItemClicked(int pos);
        public void onSwitchClicked(Boolean clicked);
    }

    public void onDone(Muzicar m){
        muzicar = m;
        //String ime = muzicar.getIme();
        //ime.setText(muzicar.getIme());
    }

}
