package ba.unsa.etf.rma.vedad.rma17_17336;

import android.graphics.Bitmap;

/**
 * Created by vedo on 3/21/18.
 */

public class Zanr {

    private String naziv;
    private Bitmap slika;

    Zanr(String naziv, Bitmap slika){
        this.naziv = naziv;
        this.slika = slika;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Bitmap getSlika() {
        return slika;
    }

    public void setSlika(Bitmap slika) {
        this.slika = slika;
    }
}
