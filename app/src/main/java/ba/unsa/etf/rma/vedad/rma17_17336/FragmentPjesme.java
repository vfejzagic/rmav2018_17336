package ba.unsa.etf.rma.vedad.rma17_17336;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class FragmentPjesme extends Fragment {

    Muzicar muzicar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View iv = inflater.inflate(R.layout.pjesme_fragment, container, false);
        if(getArguments()!=null && getArguments().containsKey("muzicar")){
            muzicar = getArguments().getParcelable("muzicar");
            ListView pjesme = (ListView) iv.findViewById(R.id.pjesmeListView);
            TextView text = (TextView) iv.findViewById(R.id.songsTitle);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity().getApplicationContext(), R.layout.pjesma, R.id.pjesmaTextView, muzicar.getPjesme());
            pjesme.setAdapter(adapter);
        }
        return iv;
    }

}
