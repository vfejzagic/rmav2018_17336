package ba.unsa.etf.rma.vedad.rma17_17336;

import android.net.UrlQuerySanitizer;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class SearchArtist extends AsyncTask<String, Integer, Void> {

    private OnMuzicarSearchDone pozivatelj;
    Muzicar rez;

    public SearchArtist(OnMuzicarSearchDone p) { pozivatelj = p; }

    @Override
    protected Void doInBackground(String... params) {
        String query = null;
        String token = "BQCDP7AxD0Pqn4Oi7SFrzgYE45CDz4LMUiLzjfKERH0w5L0-MvwkbIfpyjJK59PK1wsoB1eA4sr117EbTVPgFIhMdNEyxFCtCM77iL4mxpBvkeUYR4hXXWtX4V1SFMFeQxp__z5O4ygZk0wh1reYKIo2bfz0cnHb1doXi6yIw8h5shYjTyXQS2SrMMIKBwXHg1TW1K3HyZPfCiDs1_OLV15SlI_LNLLCSdz2IjYho3JqNPkXjoWzXCVBGoF8XFMzj0p_0mgHHbNWiNimZ1PMD99PkZc";
        try{
            query = URLEncoder.encode(params[0], "utf-8");
        }
        catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        String url1 = "https://api.spotify.com/v1/search?q=" + query + "&type=artist";
        try{
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            url.openConnection();
            urlConnection.setRequestProperty("Authorization", "Bearer " + token);
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONObject artists = jo.getJSONObject("artists");
            JSONObject items = artists.getJSONObject("items");
            for(int i = 0; i < items.length(); i++)
            {
                JSONObject artist = items.getJSONObject(Integer.toString(i));
                String name = artist.getString("name");
                String artist_id = artist.getString("id");
                rez = new Muzicar(name);

            }
        }
        catch(MalformedURLException e){
            e.printStackTrace();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(JSONException e){
            e.printStackTrace();
        }


        return null;
    }

    public String convertStreamToString(InputStream in){
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null){
                sb.append(line + " ​ \n​ ");
            }
        }catch(IOException e) {
        }finally{
            try{
                in.close();
            }catch(IOException e) {
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        pozivatelj.onDone(rez);
    }

    public interface OnMuzicarSearchDone{
        public void onDone(Muzicar rez);
    }

}
