package ba.unsa.etf.rma.vedad.rma17_17336;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class DetaljiMuzicar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalji_muzicar);

        TextView naziv = (TextView) findViewById(R.id.nazivTextView);
        TextView bio = (TextView) findViewById(R.id.bioTextView);
        TextView website = (TextView) findViewById(R.id.websiteTextView);
        Button shareBtn = (Button) findViewById(R.id.shareButton);

        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(getIntent().getStringExtra("website")); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });


        ListView pjesmeLista = (ListView) findViewById(R.id.pjesmeListView);

        final ArrayList<String> pjesme = getIntent().getStringArrayListExtra("pjesme");
        pjesmeLista.setAdapter(new ArrayAdapter<String>(this, R.layout.pjesma, R.id.pjesmaTextView, pjesme));

        pjesmeLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String naziv = pjesme.get(i).toString();
                String autor = getIntent().getStringExtra("ime");
                autor += " " + getIntent().getStringExtra("prezime");
                Uri uri = Uri.parse("https://www.youtube.com/results?search_query=" + autor + " " + naziv);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        GetSetData(naziv, bio, website);

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, getIntent().getStringExtra("bio"));
                startActivity(shareIntent);
            }
        });

    }

    private void GetSetData(TextView naziv, TextView bio, TextView website){
        Intent intent = getIntent();
        naziv.setText(intent.getStringExtra("ime") + " " + intent.getStringExtra("prezime"));
        bio.setText(intent.getStringExtra("bio"));
        website.setText(intent.getStringExtra("website"));
    }
}
