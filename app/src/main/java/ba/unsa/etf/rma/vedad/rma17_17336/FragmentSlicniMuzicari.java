package ba.unsa.etf.rma.vedad.rma17_17336;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class FragmentSlicniMuzicari extends Fragment {

    ArrayList<Muzicar> muzicari;
    MuzicarAdapter muzicarAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View iv = inflater.inflate(R.layout.slicni_muzicari_fragment, container, false);

        return iv;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getArguments()!=null && getArguments().containsKey("sviMuzicari")) {
            muzicari = getArguments().getParcelableArrayList("sviMuzicari");

            ListView lista = (ListView) getView().findViewById(R.id.sMuzicariListView);
            muzicarAdapter = new MuzicarAdapter(getActivity(), R.layout.element_liste, muzicari);
            lista.setAdapter(muzicarAdapter);
        }

    }

}
