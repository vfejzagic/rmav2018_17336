package ba.unsa.etf.rma.vedad.rma17_17336;


import android.app.FragmentManager;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class FragmentDetalji extends Fragment {

    private Muzicar muzicar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View iv = inflater.inflate(R.layout.detalji_fragment, container, false);
        if(getArguments()!=null && getArguments().containsKey("muzicar")){
            muzicar = getArguments().getParcelable("muzicar");
            TextView biografija = (TextView) iv.findViewById(R.id.bioTextView);
            TextView website = (TextView) iv.findViewById(R.id.websiteTextView);
            biografija.setText(muzicar.getBiografija());
            website.setText(muzicar.getWebsite());
            /*ListView pjesme = (ListView) iv.findViewById(R.id.pjesmeListView);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity().getApplicationContext(), R.layout.pjesma, R.id.pjesmaTextView, muzicar.getPjesme());
            pjesme.setAdapter(adapter);*/
        }
        return iv;
    }

}
