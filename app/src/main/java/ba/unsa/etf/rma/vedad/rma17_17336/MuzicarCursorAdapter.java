package ba.unsa.etf.rma.vedad.rma17_17336;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import static ba.unsa.etf.rma.vedad.rma17_17336.MuzicarDBOpenHelper.MUZICAR_IME;

/**
 * Created by Student111 on 5/18/2018.
 */

public class MuzicarCursorAdapter extends ResourceCursorAdapter {

    public MuzicarCursorAdapter(Context context, int layout, Cursor c, int flags){
        super(context, layout, c, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor){
        TextView ime = (TextView) view.findViewById(R.id.Itemname);
        ime.setText(cursor.getString(cursor.getColumnIndex(MUZICAR_IME)));

        //TextView zanr = (TextView) view.findViewById(R.id.zanr);


    }

}
