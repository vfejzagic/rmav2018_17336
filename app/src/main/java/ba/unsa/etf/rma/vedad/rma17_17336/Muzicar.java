package ba.unsa.etf.rma.vedad.rma17_17336;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Student111 on 3/16/2018.
 */

public class Muzicar implements Parcelable{

    private String ime;
    public void setIme(String ime){
        this.ime = ime;
    }
    public String getIme(){
        return this.ime;
    }

    private  String prezime;
    public void setPrezime(String prezime){
        this.prezime = prezime;
    }
    public String getPrezime(){
        return this.prezime;
    }

    private Zanr zanr;
    public Zanr getZanr() {
        return zanr;
    }
    public void setZanr(Zanr zanr) {
        this.zanr = zanr;
    }

    private  String biografija;
    public void setBiografija(String bio) {
        this.biografija = bio;
    }
    public String getBiografija(){
        return this.biografija;
    }

    private  String website;
    public void setWebsite(String web){
        this.website = web;
    }
    public String getWebsite(){
        return this.website;
    }

    private ArrayList<String> pjesme;
    public ArrayList<String> getPjesme() {
        return pjesme;
    }
    public void setPjesme(ArrayList<String> pjesme) {
        this.pjesme = pjesme;
    }

    public Muzicar(String ime, String prezime, Zanr zanr, String bio, String website, ArrayList<String> pjesme){
        this.ime = ime;
        this.prezime = prezime;
        this.zanr = zanr;
        this.biografija = bio;
        this.website = website;
        this.pjesme = pjesme;
    }

    public Muzicar(Parcel in){
        ime = in.readString();
        biografija = in.readString();
        website = in.readString();
    }

    public Muzicar(String ime){
        this.ime = ime;
    }

    public static final Creator<Muzicar> CREATOR = new Creator<Muzicar>() {
        @Override
        public Muzicar createFromParcel(Parcel parcel) {
            return new Muzicar(parcel);
        }

        @Override
        public Muzicar[] newArray(int i) {
            return new Muzicar[i];
        }
    };

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags){
        dest.writeString(ime);
        dest.writeString(biografija);
        dest.writeString(website);
    }
}
