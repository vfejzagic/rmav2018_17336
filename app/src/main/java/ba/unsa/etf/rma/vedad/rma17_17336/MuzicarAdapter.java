package ba.unsa.etf.rma.vedad.rma17_17336;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Student111 on 3/16/2018.
 */

public class MuzicarAdapter extends ArrayAdapter<Muzicar>{
/*
    private int resource;
    private int textView;
    private int imageView;
    private Context context;

    MuzicarAdapter(Context context, int _resource, int _textView, int _imageView, List<Muzicar> items){
        super(context, _resource, _textView, items);
        resource = _resource;
        textView = _textView;
        imageView = _imageView;
    }*/

    private int resource;
    private Context context;

    MuzicarAdapter(Context context, int _resource, List<Muzicar> items){
        super(context, _resource, items);
        resource = _resource;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView,
                        ViewGroup parent) {
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }
        TextView textViewItem = (TextView) newView.findViewById(R.id.Itemname);
        ImageView imageViewItem = (ImageView) newView.findViewById(R.id.icon);

        Muzicar classInstance = getItem(position);
        textViewItem.setText(classInstance.getIme() + " " + classInstance.getPrezime() + "\n" + classInstance.getZanr().getNaziv());
        imageViewItem.setImageBitmap(classInstance.getZanr().getSlika());

        return newView;
    }



}

